from time import time
import requests


knmm_endpoint = 'https://keliumokestis.lt/ws/checkVignette'
locale = 'LT'
country = 'LT'

current_time = int(time())*1000


def get_reg_info(reg_nr):
    url = f"{knmm_endpoint}?locale={locale}&country={country}&regNr={reg_nr}"
    response = requests.get(url)
    if response.status_code == 200:
        json_data = response.json()
        return json_data[0] if json_data else None
    else:
        return None


def is_valid_reg_info(reg_info: dict):
    if not reg_info:
        return False
    return (reg_info['validityStart'] <= current_time <= reg_info['validityEnd'])


def main():
    results = []

    with open('reg_numbers.txt') as f:
        reg_numbers = [line.strip() for line in f.readlines()]

    for reg_nr in reg_numbers:
        reg_info = get_reg_info(reg_nr)
        is_valid = is_valid_reg_info(reg_info)
        if is_valid:
            results.append(
                {'reg_nr': reg_nr,
                 'valid': is_valid,
                 'start': reg_info['validityStartFormatted'],
                 'end': reg_info['validityEndFormatted']
                 }
            )
        else:
            results.append({'reg_nr':reg_nr,'validity': is_valid})

    for result in results:
        print(result)


if __name__ == '__main__':
    main()
